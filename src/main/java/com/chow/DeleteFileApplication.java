package com.chow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DeleteFileApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeleteFileApplication.class, args);
    }

}
