package com.chow.updateFileName.dao;


import com.chow.updateFileName.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class FileDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 获取所有User信息
     * @return
     */
    public List<User> getUserList(){
        String sql = "select * from hack";

        List<User> users = this.jdbcTemplate.query(sql,  new RowMapper<User>() {
            @Override
            public User mapRow(ResultSet rs, int i) throws SQLException {
                User s = new User();
                s.setId(rs.getInt("id"));
                s.setNamesn(rs.getString("namesn"));
                s.setName(rs.getString("name"));
                return s;
            }
        });
        return users;
    }


    /**
     * 通过文件路径直接修改文件名
     *
     * @param filePath    需要修改的文件的完整路径
     * @param newFileName 需要修改的文件的名称
     * @return
     */
    public String fixFileName(String filePath, String newFileName) {
        File f = new File(filePath);
        System.out.println(f.getPath());

        newFileName = newFileName.trim();
        if ("".equals(newFileName) || newFileName == null) // 文件名不能为空
            return null;
        String newFilePath = null;
        if (f.isDirectory()) { // 判断是否为文件夹
            newFilePath = filePath.substring(0, filePath.lastIndexOf("/")) + "/" + newFileName;
        } else {
            newFilePath = filePath.substring(0, filePath.lastIndexOf("/")) + "/" + newFileName
                    + filePath.substring(filePath.lastIndexOf("."));
        }
        File nf = new File(newFilePath);
        try {
            f.renameTo(nf); // 修改文件名
        } catch (Exception err) {
            err.printStackTrace();
            return null;
        }
        return newFilePath;
    }
}
