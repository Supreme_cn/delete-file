package com.chow.updateFileName.bean;

public class User {
    private int id;
    private String namesn;
    private String name;

    public User() {
    }

    public User(int id, String namesn, String name) {
        this.id = id;
        this.namesn = namesn;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamesn() {
        return namesn;
    }

    public void setNamesn(String namesn) {
        this.namesn = namesn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
