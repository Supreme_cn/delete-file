package com.chow.deletefile.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class Scheduler {
    private static String FILE_PATH = "E:\\Chows\\TestDelete";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    //日志记录器
    Logger logger = LoggerFactory.getLogger(getClass());

    @Scheduled(fixedRate = 1000)
    public void testTasks(){

        logger.info("定时任务执行时间："+dateFormat.format(new Date()));
        File mavenRoot = new File(FILE_PATH);
        if (mavenRoot.exists()) {
            File[] files = mavenRoot.listFiles();
            if (files != null && files.length > 0) {
                for (File file : files) {
                    if(file.getName().endsWith(".txt")){
                        file.delete();
                        logger.info("已删除：" + file.getAbsolutePath());
                    }
                }
            }
        }
    }
}
